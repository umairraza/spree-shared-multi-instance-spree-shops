Deface::Override.new(:virtual_path => "spree/shared/_header",
                     :name => "logo",
                     :replace_contents => "#logo",
                     :text =>
                         "<img src='app/assets/images/spree/logo.png' alt=\"Shopsplanet\"/>"
)